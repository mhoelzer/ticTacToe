/* Whose turn is it now? Which cells have each player selected? */
const currentPlayer = "X";
const nextPlayer = "O";

const playerXSelections = new Array();
const playerOSelections = new Array();

// array of arrays to store the possible winning combinations on 3x3 grid
const winningCombinations = [
    // win in a row
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    // win in a column
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    // win in a diagonal
    [1, 5, 9],
    [3, 5, 7]
];


// makes function called handleClick; for now, logs id of clicked cell
handleClick = function(event) {
    let cell = event.target;
    console.log(cell.id);
};
// get array of all cells using doc.querySelectorAll()
const cells = document.querySelectorAll("td");
// iterates through those cells to add event listener
for(let i = 0; i < cells.length; i++) {
    cells[i].addEventListener("click", handleClick);
};